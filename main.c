/*
 * Yeah, stuff and such, this is public domain or if that doesn't work
 * in your country, you can do whatever else you want, I don't give a
 * crap.
 * 2014 - Turncoat
 */

#include <stdlib.h>
#include <stdio.h>

#ifdef _WIN32
#include <Windows.h>
#include <dinput.h>
#else
#include <unistd.h>
#endif

#include "rs232.h"
#include "jsmn.h"
#include "input.h"

#define BTN1            'a'
#define BTN1U           'b'
#define BTN2            'c'
#define BTN2U           'd'
#define BTN3            'e'
#define BTN3U           'f'
#define BTN4            'g'
#define BTN4U           'h'
#define BTN5            'i'
#define BTN5U           'j'
#define BTN6            'k'
#define BTN6U           'l'
#define BTN7            'm'
#define BTN7U           'n'
#define BTN8            'o'
#define BTN8U           'p'
#define BTN9            'q'
#define BTN9U           'r'

/* Lower or raise as needed. */
#define MAX_BUFFER      4096

int main(int argc, char *argv[])
{
    int i, n, x, cport_nr=0, bdrate=115200; /* /dev/ttyS0 (COM1 on windows) 9600 baud */
    INPUT ip;
    unsigned char buf[MAX_BUFFER];
    char input[MAX_BUFFER];
    //unsigned char init[MAX_BUFFER];  /* initialization string so we don't get the wrong device.*/
    //char *tmp = "tbb-0.1.3";

    /*
     * Windows only has 16(0-15) com ports, after that, nothing.
     * GNU/Linux switches to serial/USB.
     */
    for (cport_nr = 0; cport_nr < 16; cport_nr++)
    {
        if (!RS232_OpenComport(cport_nr, bdrate))
        {
            printf("Found comport: %d\n", cport_nr+1);
            break;
        } else if (cport_nr == 16)
            return EXIT_FAILURE;
        else continue;
    }

    //strcpy(init, tmp);
    //RS232_SendBuf(cport_nr, (unsigned char*)tmp, sizeof(tmp));

    while(1)
    {
        n = RS232_PollComport(cport_nr, buf, 4096);

        if(n > 0)
        {

            buf[n] = 0;

            for(i=0; i < n; i++)
            {
                if(buf[i] < 32)
                {
                    buf[i] = '.';
                }
            }

            /*for (x = 0; input_commands[x].button != NULL; x++)
            {
                if (!strcmp(buf, input_commands[x].button))
                {
                    printf("Got command: %s\n", buf);
                }
                else if (!strcmp(buf, input_commands[x].button_up))
                {
                    printf("Got release: %s\n", buf);
                }
            }*/
            /* TODO: Proper interpreter */
            switch (buf[0])
            {
                case BTN1:
                    printf("btn_1: received %i bytes: %s\n", n, (char *)buf);
                    ip.type = INPUT_KEYBOARD;
                    ip.ki.wScan = DIK_A;
                    ip.ki.time = 0;
                    ip.ki.dwExtraInfo = 0;

                    ip.ki.dwFlags = KEYEVENTF_SCANCODE;
                    SendInput(1, &ip, sizeof(INPUT));
                break;

                case BTN1U:
                    ip.ki.dwFlags |= KEYEVENTF_KEYUP;
                    SendInput(1, &ip, sizeof(INPUT));
                    printf("btn_1: btn_1up\n");
                break;

                case BTN2:
                    printf("btn_2: received %i bytes: %s\n", n, (char *)buf);
                    ip.type = INPUT_KEYBOARD;
                    ip.ki.wScan = DIK_S;
                    ip.ki.time = 0;
                    ip.ki.dwExtraInfo = 0;

                    ip.ki.dwFlags = KEYEVENTF_SCANCODE;
                    SendInput(1, &ip, sizeof(INPUT));
                break;

                case BTN2U:
                    ip.ki.dwFlags |= KEYEVENTF_KEYUP;
                    SendInput(1, &ip, sizeof(INPUT));
                    printf("btn_2: btn_2up\n");
                break;

                case BTN3:
                    printf("btn_3: received %i bytes: %s\n", n, (char *)buf);
                    ip.type = INPUT_KEYBOARD;
                    ip.ki.wScan = DIK_D;
                    ip.ki.time = 0;
                    ip.ki.dwExtraInfo = 0;

                    ip.ki.dwFlags = KEYEVENTF_SCANCODE;
                    SendInput(1, &ip, sizeof(INPUT));
                break;

                case BTN3U:
                    ip.ki.dwFlags |= KEYEVENTF_KEYUP;
                    SendInput(1, &ip, sizeof(INPUT));
                    printf("btn_3: btn_3up\n");
                break;

                case BTN4:
                    printf("btn_4: received %i bytes: %s\n", n, (char *)buf);
                    ip.type = INPUT_KEYBOARD;
                    ip.ki.wScan = DIK_F;
                    ip.ki.time = 0;
                    ip.ki.dwExtraInfo = 0;

                    ip.ki.dwFlags = KEYEVENTF_SCANCODE;
                    SendInput(1, &ip, sizeof(INPUT));
                break;

                case BTN4U:
                    ip.ki.dwFlags |= KEYEVENTF_KEYUP;
                    SendInput(1, &ip, sizeof(INPUT));
                    printf("btn_4: btn_4up\n");
                break;

                case BTN5:
                    printf("btn_5: received %i bytes: %s\n", n, (char *)buf);
                    ip.type = INPUT_KEYBOARD;
                    ip.ki.wScan = DIK_G;
                    ip.ki.time = 0;
                    ip.ki.dwExtraInfo = 0;

                    ip.ki.dwFlags = KEYEVENTF_SCANCODE;
                    SendInput(1, &ip, sizeof(INPUT));
                break;

                case BTN5U:
                    ip.ki.dwFlags |= KEYEVENTF_KEYUP;
                    SendInput(1, &ip, sizeof(INPUT));
                    printf("btn_5: btn_5up\n");
                break;

                case BTN6:
                    printf("btn_6: received %i bytes: %s\n", n, (char *)buf);
                    ip.type = INPUT_KEYBOARD;
                    ip.ki.wScan = DIK_H;
                    ip.ki.time = 0;
                    ip.ki.dwExtraInfo = 0;

                    ip.ki.dwFlags = KEYEVENTF_SCANCODE;
                    SendInput(1, &ip, sizeof(INPUT));
                break;

                case BTN6U:
                    ip.ki.dwFlags |= KEYEVENTF_KEYUP;
                    SendInput(1, &ip, sizeof(INPUT));
                    printf("btn_6: btn_6up\n");
                break;

                case BTN7:
                    printf("btn_7: received %i bytes: %s\n", n, (char *)buf);
                    ip.type = INPUT_KEYBOARD;
                    ip.ki.wScan = DIK_J;
                    ip.ki.time = 0;
                    ip.ki.dwExtraInfo = 0;

                    ip.ki.dwFlags = KEYEVENTF_SCANCODE;
                    SendInput(1, &ip, sizeof(INPUT));
                break;

                case BTN7U:
                    ip.ki.dwFlags |= KEYEVENTF_KEYUP;
                    SendInput(1, &ip, sizeof(INPUT));
                    printf("btn_7: btn_7up\n");
                break;

                case BTN8:
                    printf("btn_8: received %i bytes: %s\n", n, (char *)buf);
                    ip.type = INPUT_KEYBOARD;
                    ip.ki.wScan = DIK_K;
                    ip.ki.time = 0;
                    ip.ki.dwExtraInfo = 0;

                    ip.ki.dwFlags = KEYEVENTF_SCANCODE;
                    SendInput(1, &ip, sizeof(INPUT));
                break;

                case BTN8U:
                    ip.ki.dwFlags |= KEYEVENTF_KEYUP;
                    SendInput(1, &ip, sizeof(INPUT));
                    printf("btn_8: btn_8up\n");
                break;

                case BTN9:
                    printf("btn_9: received %i bytes: %s\n", n, (char *)buf);
                    ip.type = INPUT_KEYBOARD;
                    ip.ki.wScan = DIK_L;
                    ip.ki.time = 0;
                    ip.ki.dwExtraInfo = 0;

                    ip.ki.dwFlags = KEYEVENTF_SCANCODE;
                    SendInput(1, &ip, sizeof(INPUT));
                break;

                case BTN9U:
                    ip.ki.dwFlags |= KEYEVENTF_KEYUP;
                    SendInput(1, &ip, sizeof(INPUT));
                    printf("btn_9: btn_9up\n");
                break;

                default:
                    //printf("default: received %i bytes: %s\n", n, (char *)buf);
                break;
            }
        }
#ifdef _WIN32
        Sleep(100);
#else
        usleep(100000);  /* sleep for 100 milliSeconds */
#endif
    }
    return EXIT_SUCCESS;
}
