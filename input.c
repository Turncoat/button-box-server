/*
 * Yeah, stuff and such, this is public domain or if that doesn't work
 * in your country, you can do whatever else you want, I don't give a
 * crap.
 * 2014 - Turncoat
 */

#include <stdlib.h>
#include <stdio.h>

#ifdef _WIN32
#include <Windows.h>
#include <dinput.h>
#else
#include <unistd.h>
#endif

#include "input.h"

const struct input_data input_commands[] = {
    { "button01", "button01u",	false},
    { "button02", "button02u",	false},
    { "button03", "button03u",	false},
    { "button04", "button04u",	false},
    { "button05", "button05u",	false},
    { "button06", "button06u",	false},
    { "button07", "button07u",	false},
    { "button08", "button08u",	false},
    { "button09", "button09u",	false},
    { "", "", 0}
};

