## Compile with -O0 if you're going to run valgrind
## It adds extra debugging information.

TARGET  = tbb
CC      = gcc
PROF    = -g3 $(-O0)
C_FLAGS = -Wall $(-Werror)
L_FLAGS =

C_FILES := $(wildcard *.c)
O_FILES := $(patsubst %.c,%.o, $(wildcard *.c))

all: $(TARGET)

$(TARGET): $(O_FILES)
	@echo \(`date +%T`\) Finished compiling.
	@$(CC) $(L_FLAGS) $(O_FILES) -o $(TARGET)

.c.o:
	@echo \(`date +%T`\) Compiling $< ...
	@$(CC) -c $(PROF) $(C_FLAGS) $< -o $@

clean:
	@echo Cleaning code...
	@rm -f *.o $(TARGET)

realclean:
	@echo "Cleaning and removing backup files..."
	@rm -f *.o *.c~ *.h~ $(TARGET)

backup:
	./mkbackup